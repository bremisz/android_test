package com.example.bremisz.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.animation.LayoutTransition;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;


public class ActivityContact extends AppCompatActivity {
    EditText contact_edittext1, contact_edittext2;
    Button contact_button;
    LinearLayout linearLayout;
    Boolean isReadyToScroll;
    Toolbar tb_maintoolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        getViews();
        setActionBar();
        setActionMethods();
        //spinner
        Spinner contact_spinner = (Spinner) findViewById(R.id.contact_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.contact_spinner_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        contact_spinner.setAdapter(adapter);
    }

    private void getViews() {
        contact_edittext1 = (EditText)findViewById(R.id.contact_edittext1);
        contact_edittext2 = (EditText)findViewById(R.id.contact_edittext2);
        contact_button = (Button)findViewById(R.id.contact_button);
        linearLayout = (LinearLayout)findViewById(R.id.block_content);
        tb_maintoolbar = (Toolbar) findViewById(R.id.tb_maintoolbar);
    }

    public void setActionBar() {
        setSupportActionBar(tb_maintoolbar);
        tb_maintoolbar.setNavigationIcon(R.drawable.ico_back);
    }

    private void hideKeyboard(View view){
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setActionMethods() {

        contact_edittext1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    hideKeyboard(v);
                }
            }
        });
        contact_edittext2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    hideKeyboard(v);
                }
            }
        });
        contact_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        LayoutTransition layoutTransition = linearLayout.getLayoutTransition();
        layoutTransition.addTransitionListener(new LayoutTransition.TransitionListener() {
            @Override
            public void startTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {}

            @Override
            public void endTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
                if (view.equals(contact_button)) {
                    isReadyToScroll = true;
                    contact_edittext1.requestFocus();
                }
            }
        });
    }

    private void submit() {
        String text = contact_edittext2.getText().toString();
        if (text.matches("")) {
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                contact_edittext2.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.edittext_border_invalid, null));
                contact_edittext2.setHintTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
            } else {
                contact_edittext2.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.edittext_border_invalid, null));
                contact_edittext2.setHintTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
            }
            return;
        } else {
            Intent pinScreen = new Intent(ActivityContact.this, ActivityPin.class);
            startActivity(pinScreen);
        }
    }

}
