package com.example.bremisz.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.animation.LayoutTransition;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.view.WindowManager;
import android.text.TextWatcher;
import android.text.Editable;
import android.text.Selection;


public class ActivityPin extends AppCompatActivity {
    EditText pin_edittext1, pin_edittext2, pin_edittext3, pin_edittext4;
    Button pin_button;
    LinearLayout linearLayout, pin_edittextGroup;
    Boolean isReadyToScroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        getViews();
        setActionMethods();
    }

    private void getViews() {
        pin_edittext1 = (EditText)findViewById(R.id.pin_edittext1);
        pin_edittext2 = (EditText)findViewById(R.id.pin_edittext2);
        pin_edittext3 = (EditText)findViewById(R.id.pin_edittext3);
        pin_edittext4 = (EditText)findViewById(R.id.pin_edittext4);
        pin_button = (Button)findViewById(R.id.pin_button);
        linearLayout = (LinearLayout)findViewById(R.id.block_content);
        pin_edittextGroup = (LinearLayout)findViewById(R.id.pin_edittextGroup);
    }

    private void hideKeyboard(View view){
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setActionMethods() {
        pin_edittext1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                pin_edittext2.requestFocus();
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) { }
        });
        pin_edittext2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                pin_edittext3.requestFocus();
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) { }
        });
        pin_edittext3.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                pin_edittext4.requestFocus();
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void afterTextChanged(Editable s) { }
        });

        pin_edittext1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean focusedittexts) {
                if (focusedittexts()) {
                    hideKeyboard(v);
                }
            }
        });
        pin_edittext2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean focusedittexts) {
                if (focusedittexts()) {
                    hideKeyboard(v);
                }
            }
        });
        pin_edittext3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean focusedittexts) {
                if (focusedittexts()) {
                    hideKeyboard(v);
                }
            }
        });
        pin_edittext4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean focusedittexts) {
                if (focusedittexts()) {
                    hideKeyboard(v);
                }
            }
        });

        pin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm();
            }
        });

        LayoutTransition layoutTransition = linearLayout.getLayoutTransition();
        layoutTransition.addTransitionListener(new LayoutTransition.TransitionListener() {
            @Override
            public void startTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {}

            @Override
            public void endTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
                if (view.equals(pin_button)) {
                    isReadyToScroll = true;
                    pin_edittext1.requestFocus();
                }
            }
        });
    }

    private boolean focusedittexts(){
        if (!pin_edittext1.hasFocus() && !pin_edittext2.hasFocus() && !pin_edittext3.hasFocus() && !pin_edittext4.hasFocus()) {
            return true;
        }
        else {
            return false;
        }
    }

    private void confirm(){
         Intent tutorialScreen = new Intent(ActivityPin.this, ActivityBookmark.class);
         startActivity(tutorialScreen);
    }

}
