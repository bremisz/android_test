/*
 * Copyright (c) 2015 Capgemini Polska Sp. z o.o. All rights reserved.
 */

//needs:
//attrs.xml
//view_tabview.xml

package com.example.bremisz.myapplication;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.util.TypedValue;
import java.util.ArrayList;
import android.support.v4.content.res.ResourcesCompat;

/**
 * Displays a list of views horizontally as pages, allowing slide navigation
 * and displaying a bullet pager showing the current selected page
 */
public class CGPagerView extends LinearLayout {

    private OnCGPagerViewListener mListener;

    private boolean buttonIndicatorsAtBottom = false;
    private ViewPager vp_pager;
    private LinearLayout ll_tabs;
    private int countTabs = 0;
    private float viewWidth;
    private final int tabHeight = 30;

    public CGPagerView(Context context) {
        super(context);
        init();
    }

    public CGPagerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        setValuesByAttrs(attrs);
    }

    public CGPagerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        setValuesByAttrs(attrs);
    }

    public static int GetPixelsFromDips(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    private void init() {
        inflate(getContext(), R.layout.view_tabview, this);
        vp_pager = new ViewPager(getContext());
        LayoutParams pagerParams = new LayoutParams(LayoutParams.MATCH_PARENT, 0);
        pagerParams.weight = 1;
        vp_pager.setLayoutParams(pagerParams);
        ll_tabs = new LinearLayout(getContext());
        ll_tabs.setGravity(Gravity.CENTER);
        ll_tabs.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, GetPixelsFromDips(getContext(), tabHeight)));
    }

    private void setValuesByAttrs(AttributeSet attr) {
        TypedArray a = getContext().obtainStyledAttributes(attr, R.styleable.CGPagerView, 0, 0);
        try {
            int value = a.getInt(R.styleable.CGPagerView_pagerindicators_position, 2); //see attrs.xml definition
            if (value == 1)
                buttonIndicatorsAtBottom = false;
        } finally {
            a.recycle();
        }
    }

    public void setTabs(ArrayList<View> views) {
        countTabs = views.size();
        for (View v :views) {
            vp_pager.addView(v);
        }
        setTabsViews();
        vp_pager.setAdapter(new CGTabViewPagerAdapter());
        vp_pager.setOffscreenPageLimit(countTabs);
        vp_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                SetTabStylesBySelectionStatus(position);
                if (mListener != null) {
                    mListener.onPageChanged(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
        });
    }


    private void setTabsViews() {
        int indicatorButtonWidth = 10;
        int indicatorButtonPadding = 5;
        LinearLayout.LayoutParams paramsButtons = new LayoutParams(GetPixelsFromDips(getContext(), (indicatorButtonWidth + (indicatorButtonPadding * 2)) * countTabs), GetPixelsFromDips(getContext(), tabHeight));
        paramsButtons.gravity = Gravity.CENTER_HORIZONTAL;
        ll_tabs.setLayoutParams(paramsButtons);
        if (viewWidth > 0) {
            for (int i = 0; i < countTabs; i++) {
                Button btTab = new Button(getContext());
                LayoutParams paramsButton = new LayoutParams(GetPixelsFromDips(getContext(), indicatorButtonWidth), GetPixelsFromDips(getContext(), indicatorButtonWidth));
                paramsButton.setMargins(indicatorButtonPadding, 0, indicatorButtonPadding, 0);
                btTab.setLayoutParams(paramsButton);
                btTab.setGravity(Gravity.CENTER);
                btTab.setTag(i);
                btTab.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        SetTabStylesBySelectionStatus((int) v.getTag());
                        vp_pager.setCurrentItem((int) v.getTag(), true);
                    }
                });
                ll_tabs.addView(btTab);
            }
            SetTabStylesBySelectionStatus(0);
            if (buttonIndicatorsAtBottom) {
                addView(vp_pager);
                addView(ll_tabs);
            }
            else {
                addView(ll_tabs);
                addView(vp_pager);
            }
        }
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        viewWidth = xNew;
        if (ll_tabs.getChildCount() == 0) {
            post(new Runnable() {
                @Override
                public void run() {
                    setTabsViews();
                }
            });
        }
    }

    private void SetTabStylesBySelectionStatus(int tabSelectedIndex) {
        for (int i = 0; i < ll_tabs.getChildCount(); i++) {
            Button tab = (Button) ll_tabs.getChildAt(i);
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if ((int) tab.getTag() == tabSelectedIndex) {
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    tab.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_pagerindicatorselectedbutton, null));
                } else {
                    tab.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_pagerindicatorselectedbutton, null));
                }
            }
            else {
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    tab.setBackgroundDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_pagerindicatornotselectedbutton, null));
                } else {
                    tab.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_pagerindicatornotselectedbutton, null));
                }
            }
        }
    }


    public void setOnEventListener(OnCGPagerViewListener listener) {
        mListener = listener;
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mListener = null;
    }

    public interface OnCGPagerViewListener {

        void onPageChanged(int position);

    }

    private class CGTabViewPagerAdapter extends PagerAdapter {

        private boolean doNotifyDataSetChangedOnce = false;

        public Object instantiateItem(ViewGroup collection, int position) {
            return vp_pager.getChildAt(position);
        }

        @Override
        public int getCount() {

            if (doNotifyDataSetChangedOnce) {
                doNotifyDataSetChangedOnce = false;
                notifyDataSetChanged();
            }

            return countTabs;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }
    }
}


