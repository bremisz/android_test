package com.example.bremisz.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.view.View;
//import com.example.bremisz.myapplication.CGPagerView;

public class ActivityBookmark extends AppCompatActivity {
    Button bookmark_button;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        getViews();
        setActionMethods();

        /*CGPagerView pagerView  = (CGPagerView)findViewById(R.id.viewPager);
        pagerView.setOnEventListener(new CGPagerView.OnCGPagerViewListener() {
            @Override
            public void onPageChanged(int position) {
                if (position == APIManager.getInstance().getTutorialList().size()-1) {
                    bookmark_button.setEnabled(true);
                    bookmark_button.setAlpha(1.0f);
                }
                else {
                    bookmark_button.setEnabled(false);
                    bookmark_button.setAlpha(0.5f);

                }
            }
        });*/
    }


    private void getViews() {
        bookmark_button = (Button)findViewById(R.id.bookmark_button);
        linearLayout = (LinearLayout)findViewById(R.id.block_content);
    }

    private void setActionMethods() {

        bookmark_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tutorialScreen = new Intent(ActivityBookmark.this, ActivityMenu.class);
                startActivity(tutorialScreen);
            }
        });

    }

}
