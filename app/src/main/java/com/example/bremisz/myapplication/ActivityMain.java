package com.example.bremisz.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.animation.LayoutTransition;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.view.WindowManager;


public class ActivityMain extends AppCompatActivity {
    EditText main_edittext;
    Button main_button;
    LinearLayout linearLayout;
    Boolean isReadyToScroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getViews();
        setActionMethods();
    }

    private void getViews() {
        main_edittext = (EditText)findViewById(R.id.main_edittext);
        main_button = (Button)findViewById(R.id.main_button);
        linearLayout = (LinearLayout)findViewById(R.id.block_content);
    }

    private void hideKeyboard(View view){
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setActionMethods() {

        main_edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!v.hasFocus()) {
                    hideKeyboard(v);
                }
            }
        });
        main_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMail();
            }
        });

        LayoutTransition layoutTransition = linearLayout.getLayoutTransition();
        layoutTransition.addTransitionListener(new LayoutTransition.TransitionListener() {
            @Override
            public void startTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {}

            @Override
            public void endTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
                if (view.equals(main_button)) {
                    isReadyToScroll = true;
                    main_edittext.requestFocus();
                }
            }
        });
    }

    private void sendMail(){
        Intent pinScreen = new Intent(ActivityMain.this, ActivityPin.class);
        startActivity(pinScreen);
    }

}
