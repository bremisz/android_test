package com.example.bremisz.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;


public class ActivityMenu extends AppCompatActivity {
    Button menu_button_main, menu_button_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getViews();
        setActionMethods();
    }

    private void getViews() {
        menu_button_contact = (Button)findViewById(R.id.menu_button_contact);
        menu_button_main = (Button)findViewById(R.id.menu_button_main);
    }

    private void setActionMethods() {

        menu_button_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pinScreen = new Intent(ActivityMenu.this, ActivityContact.class);
                startActivity(pinScreen);
            }
        });
        menu_button_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pinScreen = new Intent(ActivityMenu.this, ActivityMain.class);
                startActivity(pinScreen);
            }
        });

    }

}
